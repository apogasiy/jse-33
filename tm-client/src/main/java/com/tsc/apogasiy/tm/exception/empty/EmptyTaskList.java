package com.tsc.apogasiy.tm.exception.empty;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyTaskList extends AbstractException {

    public EmptyTaskList() {
        super("Error! Task list is empty!");
    }

}
