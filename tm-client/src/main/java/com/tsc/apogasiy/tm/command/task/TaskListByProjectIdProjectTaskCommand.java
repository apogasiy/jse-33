package com.tsc.apogasiy.tm.command.task;

import com.tsc.apogasiy.tm.command.AbstractProjectTaskCommand;
import com.tsc.apogasiy.tm.endpoint.Project;
import com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TaskListByProjectIdProjectTaskCommand extends AbstractProjectTaskCommand {

    @Override
    public @NotNull String getCommand() {
        return "task-list-by-project-id";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Display list of tasks by project id";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectEndpoint().findByIdProject(getSession(), projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProjectTasks(project);
    }

}
