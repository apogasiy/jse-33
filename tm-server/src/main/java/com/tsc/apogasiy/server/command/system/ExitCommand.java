package com.tsc.apogasiy.server.command.system;

import com.tsc.apogasiy.server.command.AbstractCommand;
import com.tsc.apogasiy.server.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ExitCommand extends AbstractCommand {

    public static final String NAME = "exit";

    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public @NotNull String getCommand() {
        return NAME;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Close application";
    }

    @Override
    public void execute() {
    }

}
